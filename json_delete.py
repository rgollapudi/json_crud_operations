import json
file_name = 'json_address1.json'

with open(file_name, 'r') as f:
    my_list = json.load(f)
    print(my_list)

    for obj in (my_list):
        if "reshma" in obj:
            del obj["reshma"]

with open(file_name, 'w') as f:
    f.write(json.dumps(my_list, indent=2))